package pe.santillan.bufata.BufataRestApi.data.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import pe.santillan.bufata.BufataRestApi.data.dao.schema.DatabaseConnection;

public abstract class BaseDAO {

	protected final String TAG = BaseDAO.class.getSimpleName();
	protected final String GET_CREATED_USER = "" + "SELECT * FROM user_accounts AS users "
			+ "INNER JOIN sessions ON users.sessions_id = sessions.id "
			+ "INNER JOIN account_types ON account_types.id = users.account_types_id " + "WHERE users.id=?;" + "";
	protected final String GET_USERS = "SELECT * FROM bufata.user_accounts limit ?;";

	private final DatabaseConnection database;

	public BaseDAO() {
		database = DatabaseConnection.getInstance();
	}

	public Connection connect() {
		return database.connectToDatabase();
	}

	public void closeOperation(ResultSet resultSet, Statement stmt) {
		// it is a good idea to release
		// resources in a finally{} block
		// in reverse-order of their creation
		// if they are no-longer needed

		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException sqlEx) {
			} // ignore

			resultSet = null;
		}

		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			stmt = null;
		}
	}
}
