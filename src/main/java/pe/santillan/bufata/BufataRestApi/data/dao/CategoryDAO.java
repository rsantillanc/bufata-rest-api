package pe.santillan.bufata.BufataRestApi.data.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pe.santillan.bufata.BufataRestApi.entity.Category;

public final class CategoryDAO extends BaseDAO{

	public static final Map<Integer, Category> categoryList = new HashMap<>();

	public CategoryDAO() {
		super();
	}

	public static Category newCategory(Category category) {
		category.setId(categoryList.size() + 1);
		categoryList.put(category.getId(), category);
		return category;
	}

	public List<Category> getCategories() {
		final List<Category> list = new ArrayList<>();
		
		ResultSet resultSet = null;
		Statement stmt = null;
		
		try (final Connection conection = connect()){
			stmt = conection.createStatement();
			resultSet = stmt.executeQuery("SELECT * FROM categories");
			
			while(resultSet.next()) {
				Category cat = new Category();
				cat.setId(resultSet.getInt(1));
				cat.setCatName(resultSet.getString(2));
				cat.setDescription(resultSet.getString(3));
				
				list.add(cat);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			closeOperation(resultSet,stmt);
		}
		return list;
	}

	public static Category deleteCategory(int categoryId) {
		// TODO Auto-generated method stub
		return categoryList.remove(categoryId);
	}

	public static Category updateCategory(Category category) {
		// TODO Auto-generated method stub
		return categoryList.replace(category.getId(), category);
	}

}
