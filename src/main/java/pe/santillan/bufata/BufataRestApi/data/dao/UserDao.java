package pe.santillan.bufata.BufataRestApi.data.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import pe.santillan.bufata.BufataRestApi.entity.AccountType;
import pe.santillan.bufata.BufataRestApi.entity.ResultTransaction;
import pe.santillan.bufata.BufataRestApi.entity.Session;
import pe.santillan.bufata.BufataRestApi.entity.Social;
import pe.santillan.bufata.BufataRestApi.entity.User;
import pe.santillan.bufata.BufataRestApi.entity.UserFull;
import pe.santillan.bufata.BufataRestApi.entity.bean.UserBean;
import pe.santillan.bufata.BufataRestApi.utils.Log;
import pe.santillan.bufata.BufataRestApi.utils.ParseDate;

public class UserDao extends BaseDAO {

	public UserDao() {
		super();
	}

	public ResultTransaction<UserFull> insertAndFetchUser(UserBean _user) {
		ResultSet rs = null;
		CallableStatement stmt = null;

		Log.debug(TAG, "UserBean input: " + _user.toString());

		try (final Connection connection = connect()) {
			final String spQuery = "{CALL sp_new_user(?,?,?,?,?,?,?)}";
			stmt = connection.prepareCall(spQuery);

			// Putting sp params
			stmt.setInt(1, _user.getId());
			stmt.setString(2, _user.getEmail());
			stmt.setString(3, _user.getUsername());
			stmt.setString(4, _user.getToken());
			stmt.setInt(5, _user.getAccountTypeId());
			stmt.registerOutParameter(6, Types.INTEGER);
			stmt.registerOutParameter(7, Types.NVARCHAR);

			rs = stmt.executeQuery();
			final int userId = stmt.getInt(6);
			final String error = stmt.getString(7);
			Log.debug(TAG, "User id: " + userId);

			if (userId > 0)
				return this.fetchUser(userId);
			else
				return new ResultTransaction<UserFull>(null, error);

		} catch (SQLException ex) {
			Log.error(TAG, ex);
			return new ResultTransaction<UserFull>(null, "Fail: " + ex.getMessage());
		} finally {
			closeOperation(rs, stmt);
		}
	}

	/**
	 * This method fetch user data by id.
	 * 
	 * @param userId user identifier.
	 * @return ResultTransaction entity {@link ResultTransaction}}
	 * @throws SQLException
	 */
	public ResultTransaction<UserFull> fetchUser(int userId) throws SQLException {
		UserFull createdUser = null;
		ResultSet userRs = null;

		if (userId <= 0)
			return new ResultTransaction<UserFull>(createdUser, "We can't fetch user: " + userId);

		final Connection connection = connect();
		PreparedStatement userStmt = connection.prepareStatement(GET_CREATED_USER);
		userStmt.setInt(1, userId);
		userRs = userStmt.executeQuery();

		if (userRs.first()) {
			createdUser = new UserFull();
			createdUser.setId(userRs.getInt(1));
			createdUser.setSocial(Social.of(userRs.getString(2)));
			createdUser.setEmail(userRs.getString(3));
			createdUser.setUsername(userRs.getString(4));
			createdUser.setEnabled("1".equals(userRs.getString(5)));
			createdUser.setStatus(userRs.getInt(6));
			createdUser.setCreated(ParseDate.to(userRs.getString(7)));
			createdUser.setAccountTypeId(userRs.getInt(9));
			createdUser.setSessionId(userRs.getInt(10));

			// Create account type
			final AccountType accountType = new AccountType();
			accountType.setId(userRs.getInt(9));
			accountType.setName(userRs.getString(15));
			createdUser.setAccountType(accountType);

			// Create session
			final Session session = new Session();
			session.setId(userRs.getInt(10));
			session.setToken(userRs.getString(12));
			session.setTimeToLive(ParseDate.to(userRs.getString(13)));
			createdUser.setSession(session);

			Log.debug(TAG, "User info: " + createdUser.toString());
			closeOperation(userRs, userStmt);
			return new ResultTransaction<UserFull>(createdUser, "User created!");
		} else {
			closeOperation(userRs, userStmt);
			return new ResultTransaction<UserFull>(createdUser, "There is not result for userId: " + userId);
		}
	}

	/**
	 * This method list all users (in small).
	 * 
	 * @param limit, Limits numbers of records from database.
	 * @return list of users.
	 */
	public List<User> getUsers(int limit) {
		final List<User> users = new ArrayList<User>();
		ResultSet rs = null;
		PreparedStatement userStmt = null;
		Log.debug(TAG, "getUsers limit: " + limit);
		try (final Connection connection = connect();) {
			userStmt = connection.prepareStatement(GET_USERS);
			userStmt.setInt(1, limit); // Limit
			userStmt.execute();
			rs = userStmt.getResultSet();
			while (rs != null && rs.next()) {
				final User user = new User();
				user.setId(rs.getInt(1));
				user.setSocial(Social.of(rs.getString(2)));
				user.setEmail(rs.getString(3));
				user.setUsername(rs.getString(4));
				user.setEnabled("1".equals(rs.getString(5)));
				user.setStatus(rs.getInt(6));
				user.setCreated(ParseDate.to(rs.getString(7)));
				user.setAccountTypeId(rs.getInt(9));
				user.setSessionId(rs.getInt(10));
				users.add(user);
			}
			Log.debug(TAG, "getUsers size: " + users.size());
		} catch (SQLException e) {
			Log.error(TAG, "getUsers error:" + e.getMessage());
		} catch (RuntimeException runEx) {
			Log.error(TAG, "getUsers error:" + runEx.getMessage());
		} finally {
			closeOperation(rs, userStmt);
		}
		return users;
	}


	public UserFull getUser(int userId) {
		try {
			return fetchUser(userId).getData();
		} catch (SQLException e) {
			Log.error(TAG, "getUser error:" + e.getMessage());
		}
		return null;
	}
}
