package pe.santillan.bufata.BufataRestApi.data.dao.schema;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {

	private static DatabaseConnection instance = null;
	private Connection connection;

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	public static synchronized DatabaseConnection getInstance() {
		if (instance == null)
			instance = new DatabaseConnection();
		return instance;
	}

	public Connection connectToDatabase() {
	
		try {
			if (getConnection() != null&&!getConnection().isClosed()) {
				return getConnection();
			}
			
			MySqlConnection mysql = new MySqlConnection.Builder()
					.putHost("localhost:3306/bufata")
					.putUser("root")
					.putPassword("breveteA1")
					.build();
			System.out.println("SQL connection url: " + mysql.connect());
			setConnection(DriverManager.getConnection(mysql.connect()));
		} catch (SQLException ex) {
			setConnection(null);
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
		return getConnection();
	}

}
