package pe.santillan.bufata.BufataRestApi.data.dao.schema;

import java.util.TimeZone;

public final class MySqlConnection {
	private final String protocol = "jdbc:mysql://";
	private String host;
	private String user;
	private String password;
	private final String timeZone = "?serverTimezone=" + TimeZone.getDefault().getID();
	
	
	public static class Builder{
		private String host;
		private String user;
		private String password;
		
		public Builder putHost(String host) {
			this.host = host;
			return this;
		}
		
		public Builder putUser(String user) {
			this.user = user;
			return this;
		}
		
		public Builder putPassword(String password) {
			this.password = password;
			return this;
		}
		
		
		public MySqlConnection build() {
			MySqlConnection con = new MySqlConnection();
			con.host = this.host;
			con.user = this.user;
			con.password = this.password;
			return con;
		}
	}
	
	
	private MySqlConnection() {
		super();
	}

	public String connect() {
		StringBuilder builder = new StringBuilder();
		builder.append(protocol);
		builder.append(host);
		builder.append(timeZone);
		builder.append("&");
		builder.append("user=");
		builder.append(user);
		builder.append("&");
		builder.append("password=");
		builder.append(password);
		return builder.toString();
	}

	@Override
	public String toString() {
		return "MySqlConnection [protocol=" + protocol + ", host=" + host + ", user=" + user + ", password=" + password
				+ "]";
	}

	public String getTimeZone() {
		return timeZone;
	}
	
	
}
