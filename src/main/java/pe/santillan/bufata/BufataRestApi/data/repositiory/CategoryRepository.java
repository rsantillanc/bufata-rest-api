package pe.santillan.bufata.BufataRestApi.data.repositiory;


import java.util.List;

import pe.santillan.bufata.BufataRestApi.data.dao.CategoryDAO;
import pe.santillan.bufata.BufataRestApi.entity.Category;

public final class CategoryRepository {

	public List<Category> getCategories() {
		CategoryDAO catDao = new CategoryDAO();
		return catDao.getCategories();
	}

	public Category add(Category category) {
		return CategoryDAO.newCategory(category);
	}

	public Category update(Category category) {
		return CategoryDAO.updateCategory(category);
	}

	public Category delete(int categoryId) {
		return CategoryDAO.deleteCategory(categoryId);
	}
	
	
}
