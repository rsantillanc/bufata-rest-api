package pe.santillan.bufata.BufataRestApi.data.repositiory;

import java.util.List;

import pe.santillan.bufata.BufataRestApi.data.dao.UserDao;
import pe.santillan.bufata.BufataRestApi.entity.ResultTransaction;
import pe.santillan.bufata.BufataRestApi.entity.User;
import pe.santillan.bufata.BufataRestApi.entity.UserFull;
import pe.santillan.bufata.BufataRestApi.entity.bean.UserBean;
import pe.santillan.bufata.BufataRestApi.exception.UserException;
import pe.santillan.bufata.BufataRestApi.exception.UserException.By;
import pe.santillan.bufata.BufataRestApi.utils.Log;
import pe.santillan.bufata.BufataRestApi.utils.ValidatorUtils;

public class UserRepository {
	protected final String TAG = UserRepository.class.getSimpleName();

	/**
	 * Este método invoca al DAO de usuario e inserta el la entidad {@link User} a
	 * la base de datos y retorna la data completa del usuario registrado
	 * recientemente
	 * 
	 * @param user
	 * @return User completo
	 */
	public UserFull createUser(UserBean userRequest) {
		Log.debug(TAG, "UserBean: " + userRequest.toString());

		if (!ValidatorUtils.isUsernameValid(userRequest.getUsername())) {
			throw new UserException(By.INVALID_PARAMS, "Username must not be empty. (Min characters = 3).");
		}

		if (!ValidatorUtils.isEmailValid(userRequest.getEmail())) {
			throw new UserException(By.INVALID_PARAMS, "Invalid email. Email=" + userRequest.getEmail());
		}

		if (!ValidatorUtils.isTokenValid(userRequest.getToken())) {
			throw new UserException(By.INVALID_PARAMS, "Invalid token. Token=" + userRequest.getToken());
		}

		if (!ValidatorUtils.isAccountTypeIdValid(userRequest.getAccountTypeId())) {
			throw new UserException(By.INVALID_PARAMS,
					"Account type id must be higher than=" + userRequest.getAccountTypeId());
		}

		final UserDao userDao = new UserDao();
		ResultTransaction<UserFull> userTx = userDao.insertAndFetchUser(userRequest);
		if (userTx.getData() == null) {
			throw new UserException(By.FAIL_INSERT, userTx.getMessage());
		}
		return userTx.getData();
	}

	public List<User> getUsers(int limit) {
		final UserDao userDao = new UserDao();
		return userDao.getUsers(limit);
	}

	public UserFull getUser(int userId) {
		final UserDao userDao = new UserDao();
		UserFull user = userDao.getUser(userId);
		if (user == null)
			throw new UserException(By.USER_NOT_FOUND, "User with id=(" + userId + ") not found!");
		return user;
	}
}
