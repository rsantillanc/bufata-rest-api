package pe.santillan.bufata.BufataRestApi.entity;


public final class Category {
	private int id;
	private String name = "";
	private String description=null;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCatName() {
		return name;
	}
	public void setCatName(String catName) {
		this.name = catName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
