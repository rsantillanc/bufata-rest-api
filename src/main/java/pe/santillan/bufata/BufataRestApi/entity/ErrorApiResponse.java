package pe.santillan.bufata.BufataRestApi.entity;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public final class ErrorApiResponse {
	
	private String errorMessage;
	private int errorCode;
	private String documentation;
	private String errorInfo;
	
	public ErrorApiResponse(String errorMessage, int errorCode,String errorInfo, String documentation) {
		super();
		this.errorMessage = errorMessage;
		this.errorCode = errorCode;
		this.documentation = documentation;
		this.errorInfo = errorInfo;
	}
	

	public ErrorApiResponse() {
	
	}

	public void setDocumentation(String documentation) {
		this.documentation = documentation;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public String getDocumentation() {
		return documentation;
	}


	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}


	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}


	public String getErrorInfo() {
		return errorInfo;
	}


	public void setErrorInfo(String errorInfo) {
		this.errorInfo = errorInfo;
	}
	
	
}
