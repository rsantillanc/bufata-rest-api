package pe.santillan.bufata.BufataRestApi.entity;

public abstract class Family {
	protected int familyId;
	protected String familyName;
	protected int integrants;
	protected String address;
	protected String familyUserId;
	public int getFamilyId() {
		return familyId;
	}
	public void setFamilyId(int familyId) {
		this.familyId = familyId;
	}
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	public int getIntegrants() {
		return integrants;
	}
	public void setIntegrants(int integrants) {
		this.integrants = integrants;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getFamilyUserId() {
		return familyUserId;
	}
	public void setFamilyUserId(String familyUserId) {
		this.familyUserId = familyUserId;
	}
	
	
}
