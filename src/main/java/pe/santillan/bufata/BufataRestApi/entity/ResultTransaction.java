package pe.santillan.bufata.BufataRestApi.entity;

public class ResultTransaction<T> {

	private final T data;
	private final String message;
	
	public ResultTransaction(T data, String message) {
		this.data = data;
		this.message = message;
	}

	public T getData() {
		return data;
	}

	public String getMessage() {
		return message;
	}
	
	
}
