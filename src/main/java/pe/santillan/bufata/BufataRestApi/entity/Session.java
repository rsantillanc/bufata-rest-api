package pe.santillan.bufata.BufataRestApi.entity;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;

public class Session {
	private int id;
	private String token;
	@XmlElement(name="time_to_live")
	private Date timeToLive;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public Date getTimeToLive() {
		return timeToLive;
	}
	public void setTimeToLive(Date timeToLive) {
		this.timeToLive = timeToLive;
	}
	
	
}
