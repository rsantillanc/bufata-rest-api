package pe.santillan.bufata.BufataRestApi.entity;

public enum Social {
 GOOGLE(1,"google"),
 FACEBOOK(2,"facebook"),
 INSTAGRAM(3,"instagram"),
 OTHER(4,"other");
 
	private int index;
	private String desc;
	
	public String getDesc() {
		return desc;
	}

	public int getIndex() {
		return index;
	}
	
	Social(int index,String string){
		this.index =index;
		this.desc = string;
	}
	
	public static Social of(String desc) {
		for(int idx =0;values().length>idx;idx++) {
			if(values()[idx].desc.equals(desc))
				return values()[idx];
		}
		return null;
	}
 }
