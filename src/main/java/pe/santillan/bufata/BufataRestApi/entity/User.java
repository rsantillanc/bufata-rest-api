package pe.santillan.bufata.BufataRestApi.entity;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;

public class User {
	protected int id;
	@XmlElement(name="social_network")
	protected Social socialType;
	protected String email;
	protected String username;
	protected boolean isEnabled;
	protected int status;
	protected Date created;
	@XmlElement(name="session_id")
	private int sessionId;
	@XmlElement(name="account_type_id")
	private int accountTypeId;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Social getSocialType() {
		return socialType;
	}
	public void setSocial(Social socialType) {
		this.socialType = socialType;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public boolean isEnabled() {
		return isEnabled;
	}
	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
		;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public int getSessionId() {
		return sessionId;
	}
	public void setSessionId(int sessionId) {
		this.sessionId = sessionId;
	}
	public int getAccountTypeId() {
		return accountTypeId;
	}
	public void setAccountTypeId(int accountTypeId) {
		this.accountTypeId = accountTypeId;
	}
	public void setSocialType(Social socialType) {
		this.socialType = socialType;
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", socialType=" + socialType + ", email=" + email + ", username=" + username
				+ ", isEnabled=" + isEnabled + ", status=" + status + ", created=" + created + ", sessionId="
				+ sessionId + ", accountTypeId=" + accountTypeId + "]";
	}
	
	
	
	
}
