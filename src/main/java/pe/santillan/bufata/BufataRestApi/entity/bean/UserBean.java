package pe.santillan.bufata.BufataRestApi.entity.bean;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="user")
public class UserBean {
	private int id;
	private String email;
	private String username;
	private String token;
	@XmlElement(name="account_type_id")
	private int accountTypeId;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public int getAccountTypeId() {
		return accountTypeId;
	}
	public void setAccountTypeId(int account_type_id) {
		this.accountTypeId = account_type_id;
	}
	@Override
	public String toString() {
		return "UserBean [id=" + id + ", email=" + email + ", username=" + username + ", token=" + token
				+ ", accountTypesId=" + accountTypeId + "]";
	}
	
	
	
}
