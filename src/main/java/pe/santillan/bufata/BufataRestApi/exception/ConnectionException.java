package pe.santillan.bufata.BufataRestApi.exception;

import java.sql.SQLException;

public class ConnectionException extends SQLException {

	/**
	 * Generated
	 */
	private static final long serialVersionUID = 1L;

	private final String trace;

	public ConnectionException(String trace) {
		super(trace);
		this.trace = trace;
	}

	public String getTrace() {
		return trace;
	}
}
