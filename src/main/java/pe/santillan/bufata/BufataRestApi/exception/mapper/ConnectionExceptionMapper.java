package pe.santillan.bufata.BufataRestApi.exception.mapper;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import pe.santillan.bufata.BufataRestApi.entity.ErrorApiResponse;
import pe.santillan.bufata.BufataRestApi.exception.ConnectionException;

@Provider
public class ConnectionExceptionMapper implements ExceptionMapper<ConnectionException> {

	@Override
	public Response toResponse(ConnectionException exception) {
		ErrorApiResponse errorApi = new ErrorApiResponse();
		errorApi.setDocumentation("https://www.santillan.pe");
		errorApi.setErrorCode(Status.INTERNAL_SERVER_ERROR.getStatusCode());
		errorApi.setErrorInfo(exception.getMessage());
		errorApi.setErrorMessage("Connection error!");
		return Response.serverError().entity(errorApi).build();
	}

}
