package pe.santillan.bufata.BufataRestApi.exception.mapper;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import pe.santillan.bufata.BufataRestApi.entity.ErrorApiResponse;
import pe.santillan.bufata.BufataRestApi.exception.UserException;

@Provider
public class UserExceptionMapper implements ExceptionMapper<UserException> {

	@Override
	public Response toResponse(UserException exception) {
		ErrorApiResponse errorApi = new ErrorApiResponse();
		errorApi.setDocumentation("https://www.santillan.pe");
		errorApi.setErrorCode(Status.INTERNAL_SERVER_ERROR.getStatusCode());
		errorApi.setErrorMessage(exception.getMessage());
		Response response =  Response.status(Status.INTERNAL_SERVER_ERROR)
				.entity(errorApi)
				.build();

		switch (exception.getBy()) {
		case FAIL_INSERT:
			errorApi.setErrorCode(Status.BAD_REQUEST.getStatusCode());
			errorApi.setErrorMessage("User insert failed!");
			errorApi.setErrorInfo(exception.getMessageInfo());
			response = Response.status(Status.BAD_REQUEST)
			.entity(errorApi)
			.build();
			break;
		case INVALID_PARAMS:
			errorApi.setErrorCode(Status.BAD_REQUEST.getStatusCode());
			errorApi.setErrorMessage("Some params are required or are incorrect!");
			errorApi.setErrorInfo(exception.getMessageInfo());
			response = Response.status(Status.BAD_REQUEST)
			.entity(errorApi)
			.build();
			break;
		case USER_ALREADY_EXIST:
			errorApi.setErrorCode(Status.CONFLICT.getStatusCode());
			errorApi.setErrorMessage("User already exist!");
			errorApi.setErrorInfo(exception.getMessageInfo());
			response = Response.status(Status.CONFLICT)
			.entity(errorApi)
			.build();
			break;
		case USER_NOT_FOUND:
			errorApi.setErrorCode(Status.NOT_FOUND.getStatusCode());
			errorApi.setErrorMessage("User not found!");
			errorApi.setErrorInfo(exception.getMessageInfo());
			response = Response.status(Status.NOT_FOUND)
			.entity(errorApi)
			.build();
			break;
		default:
			break;
		}
		return response;
	}

}
