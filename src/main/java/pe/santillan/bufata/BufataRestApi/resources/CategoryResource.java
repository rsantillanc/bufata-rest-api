package pe.santillan.bufata.BufataRestApi.resources;


import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import pe.santillan.bufata.BufataRestApi.data.repositiory.CategoryRepository;
import pe.santillan.bufata.BufataRestApi.entity.Category;


@Path("/categories")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public final class CategoryResource {

	final CategoryRepository categoryRepo = new CategoryRepository();
	
	@GET
	public List<Category> getCategories(){
		 List<Category> categories = categoryRepo.getCategories();
		return categories;
	}
	
	@POST
	public Response insert(Category category){
		Category entity = categoryRepo.add(category);
		return Response.status(Status.CREATED).entity(entity).build();
	}
	
	@DELETE
	@Path("/{categoryId}")
	public Category delete(@PathParam("categoryId") int categoryId){
		return categoryRepo.delete(categoryId);
	}
	
	@PUT
	public Category update(Category category){
		return categoryRepo.update(category);
	}
}
