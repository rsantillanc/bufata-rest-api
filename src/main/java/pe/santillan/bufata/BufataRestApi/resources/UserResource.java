package pe.santillan.bufata.BufataRestApi.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import pe.santillan.bufata.BufataRestApi.data.repositiory.UserRepository;
import pe.santillan.bufata.BufataRestApi.entity.User;
import pe.santillan.bufata.BufataRestApi.entity.UserFull;
import pe.santillan.bufata.BufataRestApi.entity.bean.UserBean;

@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserResource {

	private final UserRepository userRepository = new UserRepository();

	@POST
	public Response createUser(UserBean userRequest) {
		UserFull user = userRepository.createUser(userRequest);
		return Response.status(Status.CREATED).entity(user).build();
	}

	@GET
	public List<User> getAllUsers(@DefaultValue("100") @QueryParam("limit") int limit) {
		return userRepository.getUsers(limit);
	}

	@GET
	@Path("/{user-id}")
	public Response getUser(@PathParam("user-id") int userId) {
		UserFull entity = userRepository.getUser(userId);
		return Response.status(Status.OK).entity(entity).build();
	}

}
