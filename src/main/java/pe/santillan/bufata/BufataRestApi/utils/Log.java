package pe.santillan.bufata.BufataRestApi.utils;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Log {

	public static void debug(String tag,String message) {
		Logger logger = Logger.getLogger(tag);
		logger.log(Level.INFO, message);
	}
	
	public static void debug(String tag,Throwable ex) {
		Logger logger = Logger.getLogger(tag);
		logger.log(Level.INFO, ex.getMessage());
	}
	
	public static void error(String tag,Throwable ex) {
		Logger logger = Logger.getLogger(tag);
		logger.log(Level.SEVERE, ex.getMessage());
	}

	public static void error(String tag,String message) {
		Logger logger = Logger.getLogger(tag);
		logger.log(Level.SEVERE, message);
	}
}
