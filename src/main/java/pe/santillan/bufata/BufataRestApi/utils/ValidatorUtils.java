package pe.santillan.bufata.BufataRestApi.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidatorUtils {
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = 
		    Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

	public static boolean isUsernameValid(String username) {
		return !isNullOrEmpty(username) && !" ".equals(username) && username.length() >= 3;
	}

	public static boolean isNullOrEmpty(String value) {
		return value == null || value.isEmpty();
	}

	public static boolean isEmailValid(String email) {
		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(email);
		return matcher.find();
	}

	public static boolean isTokenValid(String token) {
		return !isNullOrEmpty(token);
	}

	public static boolean isAccountTypeIdValid(int accountTypeId) {
		return accountTypeId>0;
	}
}
